package net.folivo.trixnity.client.room.outbox

data class OutboxMessageMediaUploaderMappings(val mappings: List<OutboxMessageMediaUploaderMapping<*>>)
